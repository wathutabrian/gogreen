package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	gohandlers "github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/project/GoGreen/db"
	"github.com/project/GoGreen/handlers"
)

func main() {
	//loading .env file for the sensitve data eg PORT
	err := godotenv.Load("files.env")
	if err != nil {
		log.Println(("unable to load .env file"))
		log.Fatal(err)
	}
	port := os.Getenv("PORT")

	//creating a handler to facilitate calling of the interfaces
	dbconn := db.Connect()
	l := log.New(os.Stdout, "Product-api", log.LstdFlags)
	ph := handlers.NewHandler(l, dbconn)

	defer dbconn.Close()
	mux := mux.NewRouter()
	//handles all the GET requests
	getRouter := mux.Methods(http.MethodGet).Subrouter()
	getRouter.HandleFunc("/", ph.Index)
	getRouter.HandleFunc("/search", ph.Search)

	//postRouter handles all the post requests
	postRouter := mux.Methods(http.MethodPost).Subrouter()
	postRouter.HandleFunc("/brand", ph.InsertBrand)
	postRouter.HandleFunc("/category", ph.InsertCategory)
	postRouter.HandleFunc("/product", ph.InsertProduct)

	//ensuring that the server allows CORS
	ch := gohandlers.CORS(gohandlers.AllowedOrigins([]string{"*"}))
	//customizing the Server
	s := http.Server{
		Addr:     port,
		Handler:  ch(mux),
		ErrorLog: l,
	}
	//starting the server
	go func() {
		l.Println("starting the server on port", port)
		log.Fatal(s.ListenAndServe())
	}()
	//sigchan is a channel that listens for a signal to terminate connection
	sigChan := make(chan os.Signal, 1)

	signal.Notify(sigChan, syscall.SIGTERM)
	signal.Notify(sigChan, os.Interrupt)

	c := <-sigChan
	fmt.Println("graceful shutdown ", c)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	s.Shutdown(ctx)

}
