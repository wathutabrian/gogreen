package handlers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/project/GoGreen/db"
	"github.com/project/GoGreen/models"
)

type DBHandler struct {
	L  *log.Logger
	Db *sql.DB
}

//Idiomatic founction for referncing a handler
func NewHandler(l *log.Logger, Db *sql.DB) *DBHandler {
	return &DBHandler{l, Db}
}

//Index is the handler that is called when a call is made the api with the get method
//it returns a list of all produtcs in the database
func (h *DBHandler) Index(w http.ResponseWriter, r *http.Request) {
	products, err := db.GetAllProducts()
	if err != nil {
		h.L.Println("unable to get all producs", err)
		http.Error(w, "Internal error has occured", http.StatusInternalServerError)
		return
	}
	data, err := json.Marshal(products)
	if err != nil {
		h.L.Println(err.Error())
		return
	}
	w.Header().Set("", "")
	w.Write(data)

}

//Search is the handler that is called when a call with the /search extention on the domain name is made on the request
//it takes json data that contains the name of the product to be searched
func (h *DBHandler) Search(w http.ResponseWriter, r *http.Request) {
	var Product models.Product
	var NewProduct *models.Product
	var err error
	if err := json.NewDecoder(r.Body).Decode(&Product); err != nil {
		h.L.Println("unable to get all producs", err)
		http.Error(w, "Internal error has occured", http.StatusInternalServerError)
		return
	}
	fmt.Println("Database ")
	NewProduct, err = db.GetbyName(Product.Name)
	if err != nil {
		h.L.Println("unable to get all producs", err)
		http.Error(w, "Internal error has occured", http.StatusInternalServerError)
		return
	}
	//the data from the database is encoded into json and returned as a response
	if err := json.NewEncoder(w).Encode(NewProduct); err != nil {
		h.L.Println("unable to get all producs", err)
		http.Error(w, "Internal error has occured", http.StatusInternalServerError)
		return
	}
}

//
func (h *DBHandler) InsertBrand(w http.ResponseWriter, r *http.Request) {
	var brand models.Brands
	if err := json.NewDecoder(r.Body).Decode(&brand); err != nil {
		http.Error(w, "Internal error has occured", http.StatusInternalServerError)
		h.L.Fatal(err)
	}
	db.InsertBrands(brand)
}
func (h *DBHandler) InsertCategory(w http.ResponseWriter, r *http.Request) {
	var category models.Category
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		http.Error(w, "Internal error has occured", http.StatusInternalServerError)
		h.L.Fatal(err)
	}
	db.InsertCategory(category)
}
func (h *DBHandler) InsertProduct(w http.ResponseWriter, r *http.Request) {
	var product models.Product
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		http.Error(w, "Internal error has occured", http.StatusInternalServerError)
		h.L.Fatal(err)
	}
}
