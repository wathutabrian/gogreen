package db

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/project/GoGreen/models"
)

var DB *sql.DB

//Connect returns a connection to the db
func Connect() *sql.DB {
	var err error
	DB, err = sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/GoGreen")
	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}

	fmt.Println("connection succesful")
	return DB
}

//GetAllProducts Queries the database for all the products
func GetAllProducts() ([]models.Product, error) {
	var products []models.Product
	results, err := DB.Query(`SELECT * FROM Products`)
	if err != nil {
		log.Println("ERROR has occured (unable to select from the db) ")
		return nil, err
	}
	defer results.Close()
	for results.Next() {
		var product models.Product
		if err := results.Scan(&product.ID, &product.Name, &product.Image, &product.Details, &product.Date_Added, &product.Price, &product.Quantity, &product.CategoryID, &product.BrandID); err != nil {
			return nil, err
		}
		products = append(products, product)
	}
	return products, nil
}

//GetbyId Queries the database and returns the products based on the id
func GetbyId(id int) (*models.Product, error) {
	var product models.Product
	row, err := DB.Query(fmt.Sprintf("SELECT Name,Image,Details,Date_Added,Price,Quantity,Category_ID,Brand_ID FROM Products WHERE ID='%v'", id))
	if err != nil {
		return nil, err
	}
	defer row.Close()
	for row.Next() {
		if err := row.Scan(&product.Name, &product.Image, &product.Details, &product.Date_Added, &product.Price, &product.Quantity, &product.CategoryID, &product.BrandID); err != nil {
			return nil, err
		}
	}
	product.ID = id

	return &product, nil
}

//GetbyName Query the database for a product based on the name provided
func GetbyName(name string) (*models.Product, error) {
	var product models.Product
	row, err := DB.Query(fmt.Sprintf("SELECT ID FROM Products WHERE Name='%v'", name))
	if err != nil {
		return nil, err
	}
	defer row.Close()
	for row.Next() {
		if err := row.Scan(&product.ID); err != nil {
			fmt.Println("error 3")
			return nil, err
		}
	}
	var newProduct *models.Product
	newProduct, err = GetbyId(product.ID)
	if err != nil {
		return nil, err
	}
	return newProduct, nil
}

//InsertProduct takes a product as an argument and inserts the product into the database
func InsertProduct(brand models.Brands, category models.Category, product models.Product) error {
	if err := InsertCategory(category); err != nil {
		return err
	}

	row, err := DB.Query(fmt.Sprintf("SELECT ID FROM Categories WHERE Name=%v AND Description=%v)", category.Name, category.Description))
	if err != nil {
		return err
	}
	for row.Next() {
		err := row.Scan(&category.ID)
		if err != nil {
			return err
		}
	}
	defer row.Close()

	if err := InsertBrands(brand); err != nil {
		return err
	}

	row, err = DB.Query(fmt.Sprintf("SELECT ID FROM Brands WHERE Name=%v AND Description=%v)", brand.Name, brand.Description))
	if err != nil {
		return err
	}

	for row.Next() {
		err := row.Scan(&brand.ID)
		if err != nil {
			return err
		}
	}

	defer row.Close()

	var rows *sql.Rows

	rows, err = DB.Query(fmt.Sprintf("INSERT INTO Products  SET Name=%v, Image=%v, Details=%v, Date_Added=NOW(), Price=%v, Quantity=%v, Category_ID=(SELECT ID FROM Categories WHERE Name=%v), Brand_ID=(SELECT ID FROM Brands WHERE Name=%v);", product.Name, product.Image, product.Details, product.Price, product.Quantity, category.ID, brand.ID))

	if err != nil {
		return err
	}

	defer rows.Close()

	return nil
}

//InsertCategory performs an insert Query to the database
func InsertCategory(category models.Category) error {
	row, err := DB.Query(fmt.Sprintf("INSERT INTO Categories VALUES (%v,%v,%d);", category.Name, category.Description, category.No_Of_Products))
	if err != nil {
		return err
	}
	defer row.Close()
	return nil
}

//InsertCategory performs an insert Query to the database
func InsertBrands(brand models.Brands) error {
	row, err := DB.Query(fmt.Sprintf("INSERT INTO Brands VALUES (%v,%v,%d)", brand.Name, brand.Description, brand.No_Of_Products))
	if err != nil {
		return err
	}
	defer row.Close()
	return nil
}
