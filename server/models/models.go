package models

//Users is a model of the users that will be using the app
type Users struct {
	ID       uint16 `json:"id,omitempty"`
	UserName string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
	Name     string `json:"name,omitempty"`
	Role     string `json:"role,omitempty"`
	Email    string `json:"email,omitempty"`
	Address  string `json:"address"`
	Contact  string `json:"contact"`
}

//Product is a model of the products
type Product struct {
	ID         int
	Name       string `json:"name"`
	Image      string `json:"image"`
	Details    string `json:"details"`
	Date_Added string `json:"date_added"`
	Price      string `json:"price"`
	Quantity   string `json:"quantity"`
	CategoryID int    `json:"category"`
	BrandID    int    `json:"brand"`
}
type Category struct {
	ID             uint16 `json:"cartegory_id,omitempty"`
	Name           string `json:"name,omitempty"`
	Description    string `json:"Description"`
	No_Of_Products uint   `json:"no_of_products"`
}
type Brands struct {
	ID             uint16 `json:"brand_id,omitempty"`
	Name           string `json:"name,omitempty"`
	Description    string `json:"description"`
	No_Of_Products uint   `json:"no_of_products"`
}
